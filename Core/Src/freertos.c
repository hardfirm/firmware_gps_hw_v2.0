/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
#include "fatfs.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
  FATFS fs;
  FATFS *pfs;
  FIL fil;
  FRESULT fres;
  DWORD fre_clust;
  uint32_t total, free_app;
  char buffer[100];
/* USER CODE END Variables */
/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
const osThreadAttr_t defaultTask_attributes = {
  .name = "defaultTask",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityNormal,
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void *argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* USER CODE BEGIN StartDefaultTask */
  printf("GPS START\r\n");
  HAL_GPIO_WritePin(Q_RESETN_GPIO_Port, Q_RESETN_Pin, RESET );
  osDelay(500);/*mas de 460uS*/
  HAL_GPIO_WritePin(Q_RESETN_GPIO_Port, Q_RESETN_Pin, SET );
  osDelay(500);

  HAL_GPIO_WritePin(Q_PWR_KEY_GPIO_Port, Q_PWR_KEY_Pin, RESET );
  osDelay(550);
  HAL_GPIO_WritePin(Q_PWR_KEY_GPIO_Port, Q_PWR_KEY_Pin, SET );
  osDelay(550);
  HAL_GPIO_WritePin(Q_PWR_KEY_GPIO_Port, Q_PWR_KEY_Pin, RESET );



  /* Mount SD Card */
  if(f_mount(&fs, "0:", 1) != FR_OK)
  {
	  printf("Error Mount: %s-%d\r\n",__FILE__, __LINE__);
  }


  /* Open file to write */
  if(f_open(&fil, "first.txt", FA_OPEN_ALWAYS | FA_READ | FA_WRITE) != FR_OK)
	  printf("%s-%d\r\n",__FILE__, __LINE__);

  /* Check free space */
  if(f_getfree("", &fre_clust, &pfs) != FR_OK)
	  printf("%s-%d\r\n",__FILE__, __LINE__);

  total = (uint32_t)((pfs->n_fatent - 2) * pfs->csize * 0.5);
  free_app = (uint32_t)(fre_clust * pfs->csize * 0.5);

  /* Free space is less than 1kb */
  if(free_app < 1)
	  printf("%s-%d\r\n",__FILE__, __LINE__);

  /* Writing text */
  f_puts("STM32 SD Card I/O Example via SPI\r\n", &fil);
  f_puts("Save the world!!!\r\n", &fil);

  /* Close file */
  if(f_close(&fil) != FR_OK)
	  printf("%s-%d\r\n",__FILE__, __LINE__);

  /* Open file to read */
  if(f_open(&fil, "first.txt", FA_READ) != FR_OK)
	  printf("%s-%d\r\n",__FILE__, __LINE__);

  while(f_gets(buffer, sizeof(buffer), &fil))
  {
    //printf("%s", buffer);
  }

  /* Close file */
  if(f_close(&fil) != FR_OK)
	  printf("%s-%d\r\n",__FILE__, __LINE__);

  /* Unmount SDCARD */
  if(f_mount(NULL, "0:", 1) != FR_OK)
	  printf("%s-%d\r\n",__FILE__, __LINE__);

  /* Infinite loop */
  for(;;)
  {
    osDelay(500);
    HAL_GPIO_TogglePin(LED_LIFE_GPIO_Port, LED_LIFE_Pin);
  }
  /* USER CODE END StartDefaultTask */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
