/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define FLAGB_Pin GPIO_PIN_14
#define FLAGB_GPIO_Port GPIOC
#define GPS_ANT_EN_Pin GPIO_PIN_15
#define GPS_ANT_EN_GPIO_Port GPIOC
#define VBAT_Measure_Pin GPIO_PIN_0
#define VBAT_Measure_GPIO_Port GPIOA
#define CONTACTO_Pin GPIO_PIN_1
#define CONTACTO_GPIO_Port GPIOA
#define Q_PWR_KEY_Pin GPIO_PIN_4
#define Q_PWR_KEY_GPIO_Port GPIOA
#define Q_STATUS_Pin GPIO_PIN_5
#define Q_STATUS_GPIO_Port GPIOA
#define Q_RESETN_Pin GPIO_PIN_6
#define Q_RESETN_GPIO_Port GPIOA
#define PWR_GOOD_Pin GPIO_PIN_7
#define PWR_GOOD_GPIO_Port GPIOA
#define STAT_1_Pin GPIO_PIN_0
#define STAT_1_GPIO_Port GPIOB
#define EN_4_8V_Pin GPIO_PIN_1
#define EN_4_8V_GPIO_Port GPIOB
#define BOOT1_Pin GPIO_PIN_2
#define BOOT1_GPIO_Port GPIOB
#define USB_VBUS_Pin GPIO_PIN_10
#define USB_VBUS_GPIO_Port GPIOB
#define SD_DETECT_Pin GPIO_PIN_11
#define SD_DETECT_GPIO_Port GPIOB
#define W_CH_PD_Pin GPIO_PIN_12
#define W_CH_PD_GPIO_Port GPIOB
#define W_RESET_Pin GPIO_PIN_8
#define W_RESET_GPIO_Port GPIOA
#define LED_LIFE_Pin GPIO_PIN_9
#define LED_LIFE_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
